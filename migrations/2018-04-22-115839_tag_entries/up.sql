CREATE TABLE entries_tags (
  entry_id BLOB NOT NULL,
  tag_id BLOB NOT NULL,
  PRIMARY KEY (entry_id, tag_id),
  FOREIGN KEY(entry_id) REFERENCES entries(id),
  FOREIGN KEY(tag_id) REFERENCES tags(id)
)
