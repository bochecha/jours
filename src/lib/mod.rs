/* Copyright (c) 2018 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Jours
 *
 * Jours is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jours is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Jours.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::path::Path;

use chrono::prelude::NaiveDate;

use diesel::insert_into;
use diesel::prelude::*;
use diesel::result::Error::NotFound;
use diesel::sqlite::SqliteConnection;

use failure::Error;

pub mod cli;
pub mod models;
pub mod schema;
pub mod utils;

use self::models::{Entry, Tag, Uuid};
use self::schema::{entries, entries_tags, tags};
use self::utils::NaiveDateRange;

embed_migrations!("migrations");

#[derive(Debug)]
pub struct Jours {
    db_url: String,
}

impl Jours {
    pub fn new(data_dir: &Path) -> Result<Jours, Error> {
        let db_path = data_dir.canonicalize()?.join("jours.sqlite");
        let db_url = match db_path.to_str() {
            Some(s) => s.to_owned(),
            None => bail!("Invalid DB path: {:?}", db_path),
        };

        let jours = Jours { db_url };
        jours.run_migrations()?;

        Ok(jours)
    }

    fn establish_connection(&self) -> Result<SqliteConnection, Error> {
        SqliteConnection::establish(&self.db_url).map_err(Error::from)
    }

    fn run_migrations(&self) -> Result<(), Error> {
        let connection = self.establish_connection()?;
        embedded_migrations::run(&connection)?;

        Ok(())
    }

    pub fn record_entry<'a>(
        &'a self,
        summary: &'a str,
        details: &'a str,
        date: &'a NaiveDate,
        tags: &'a [String],
    ) -> Result<(), Error> {
        let entry_id = Uuid::new();
        let connection = self.establish_connection()?;

        connection.transaction::<(), Error, _>(|| {
            insert_into(entries::table)
                .values((
                    entries::id.eq(&entry_id),
                    entries::summary.eq(summary),
                    entries::details.eq(details),
                    entries::date.eq(date),
                ))
                .execute(&connection)?;

            for tag in tags {
                let tag_id = match tags::table
                    .filter(tags::label.eq(&tag))
                    .select(tags::id)
                    .first::<Uuid>(&connection)
                {
                    Ok(id) => id,
                    Err(NotFound) => {
                        let tag_id = Uuid::new();

                        insert_into(tags::table)
                            .values((tags::id.eq(&tag_id), tags::label.eq(&tag)))
                            .execute(&connection)?;

                        tag_id
                    },
                    Err(e) => bail!("Unhandled error: {}", e),
                };

                insert_into(entries_tags::table)
                    .values((
                        entries_tags::entry_id.eq(&entry_id),
                        entries_tags::tag_id.eq(&tag_id),
                    ))
                    .execute(&connection)?;
            }

            Ok(())
        })
    }

    pub fn get_entries(
        self,
        filter_range: Option<NaiveDateRange>,
        filter_tag: Option<String>,
        limit: i64,
    ) -> Result<Vec<Entry>, Error> {
        use self::schema::entries::dsl::*;

        let connection = self.establish_connection()?;
        let query = entries.into_boxed();

        let query = match filter_tag {
            None => query,
            Some(label) => {
                let tag = match tags::table
                    .filter(tags::label.eq(&label))
                    .first::<Tag>(&connection)
                {
                    Ok(tag) => tag,
                    Err(NotFound) => bail!("No such tag: {}", label),
                    Err(e) => bail!("Unhandled error: {}", e),
                };
                let entry_ids = entries_tags::table
                    .filter(entries_tags::tag_id.eq(tag.id))
                    .select(entries_tags::entry_id)
                    .load::<Uuid>(&connection)?;

                query.filter(id.eq_any(entry_ids))
            },
        };

        let query = match filter_range {
            None => query,
            Some(range) => {
                if range.start == range.end {
                    query.filter(date.eq(range.start))
                } else {
                    query
                        .filter(date.ge(range.start))
                        .filter(date.le(range.end))
                }
            },
        };

        let query = query.order((date.desc(), id.desc()));
        let query = match limit {
            0 => query,
            _ => query.limit(limit),
        };

        query.load::<Entry>(&connection).map_err(Error::from)
    }
}
