/* Copyright (c) 2018 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Jours
 *
 * Jours is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jours is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Jours.  If not, see <http://www.gnu.org/licenses/>.
 */

use chrono::prelude::{Datelike, NaiveDate};

use failure::Error;

#[derive(Debug)]
pub struct NaiveDateRange {
    pub start: NaiveDate,
    pub end: NaiveDate,
}

// Taken from https://github.com/chronotope/chrono/issues/29#issuecomment-84492746
fn last_day_of_month(year: i32, month: u32) -> u32 {
    NaiveDate::from_ymd_opt(year, month + 1, 1)
        .unwrap_or_else(|| NaiveDate::from_ymd(year + 1, 1, 1))
        .pred()
        .day()
}

pub fn parse_date_range(range_spec: &str) -> Result<NaiveDateRange, Error> {
    match range_spec.parse::<NaiveDate>() {
        Ok(date) => Ok(NaiveDateRange {
            start: date,
            end: date,
        }),
        Err(_) => {
            // FIXME: This is pretty crude x_x
            let start_spec = format!("{}-01", range_spec);
            let start = start_spec.parse::<NaiveDate>()?;

            let year = start.year();
            let month = start.month();
            let end_day = last_day_of_month(year, month);
            let end = NaiveDate::from_ymd(year, month, end_day);

            Ok(NaiveDateRange { start, end })
        },
    }
}

#[cfg(test)]
mod test_last_day_of_month {
    use super::*;

    #[test]
    fn year_2018() {
        let month_ends = [
            (1, 31),
            (2, 28),
            (3, 31),
            (4, 30),
            (5, 31),
            (6, 30),
            (7, 31),
            (8, 31),
            (9, 30),
            (10, 31),
            (11, 30),
            (12, 31),
        ];

        for &(month, last_day) in month_ends.iter() {
            assert_eq!(last_day_of_month(2018, month), last_day);
        }
    }

    #[test]
    fn year_2020() {
        let month_ends = [
            (1, 31),
            (2, 29),
            (3, 31),
            (4, 30),
            (5, 31),
            (6, 30),
            (7, 31),
            (8, 31),
            (9, 30),
            (10, 31),
            (11, 30),
            (12, 31),
        ];

        for &(month, last_day) in month_ends.iter() {
            assert_eq!(last_day_of_month(2020, month), last_day);
        }
    }
}

#[cfg(test)]
mod test_parse_date_range {
    use super::*;

    #[test]
    fn year_month() {
        let range = parse_date_range("2018-04").unwrap();
        assert_eq!(range.start, NaiveDate::from_ymd(2018, 4, 1));
        assert_eq!(range.end, NaiveDate::from_ymd(2018, 4, 30));
    }
}
