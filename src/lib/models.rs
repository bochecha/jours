/* Copyright (c) 2018 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Jours
 *
 * Jours is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jours is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Jours.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;
use std::io::Write;

use chrono::NaiveDate;

use diesel::backend::Backend;
use diesel::deserialize::{self, FromSql};
use diesel::serialize::{self, IsNull, Output, ToSql};
use diesel::sql_types::Binary;
use diesel::sqlite::Sqlite;

use super::schema::{entries, entries_tags, tags};

use uuid;

#[derive(Debug, Default, Eq, PartialEq, Hash, FromSqlRow, AsExpression)]
#[sql_type = "Binary"]
pub struct Uuid {
    uuid: uuid::Uuid,
}

impl ToSql<Binary, Sqlite> for Uuid {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Sqlite>) -> serialize::Result {
        out.write_all(self.uuid.as_bytes())?;
        Ok(IsNull::No)
    }
}

impl FromSql<Binary, Sqlite> for Uuid {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let bytes = <Vec<u8> as FromSql<Binary, Sqlite>>::from_sql(bytes)?;

        Ok(Uuid {
            uuid: uuid::Uuid::from_bytes(&bytes)?,
        })
    }
}

impl fmt::Display for Uuid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.uuid.hyphenated().to_string())
    }
}

impl Uuid {
    pub fn new() -> Uuid {
        Uuid {
            uuid: uuid::Uuid::new_v4(),
        }
    }
}

#[derive(Identifiable, Queryable)]
#[table_name = "entries"]
pub struct Entry {
    pub id: Uuid,
    pub summary: String,
    pub details: String,
    pub date: NaiveDate,
}

#[derive(Identifiable, Queryable)]
#[table_name = "tags"]
pub struct Tag {
    pub id: Uuid,
    pub label: String,
}

#[derive(Identifiable, Queryable, Associations)]
#[belongs_to(Entry)]
#[belongs_to(Tag)]
#[primary_key(entry_id, tag_id)]
#[table_name = "entries_tags"]
pub struct EntryTag {
    pub entry_id: Uuid,
    pub tag_id: Uuid,
}
