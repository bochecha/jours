table! {
    entries (id) {
        id -> Binary,
        summary -> Text,
        details -> Text,
        date -> Date,
    }
}

table! {
    entries_tags (entry_id, tag_id) {
        entry_id -> Binary,
        tag_id -> Binary,
    }
}

table! {
    tags (id) {
        id -> Binary,
        label -> Text,
    }
}

joinable!(entries_tags -> entries (entry_id));
joinable!(entries_tags -> tags (tag_id));

allow_tables_to_appear_in_same_query!(
    entries,
    entries_tags,
    tags,
);
