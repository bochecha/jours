/* Copyright (c) 2018 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Jours
 *
 * Jours is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jours is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Jours.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;
use std::fs::create_dir_all;
use std::path::PathBuf;
use std::str::FromStr;

use chrono::prelude::{Local, NaiveDate};

use console::style;

use failure::Error;

use plume::get_text;

use structopt::StructOpt;

use textwrap::indent;

use xdg_basedir;

#[derive(Debug, StructOpt)]
#[structopt(name = "jours")]
/// Log your days
struct Cli {
    #[structopt(long = "data-dir")]
    /// The directory for Jours to store its data [default: ${XDG_DATA_HOME}/jours]
    data_dir: Option<String>,

    #[structopt(subcommand)]
    cmd: Option<Command>,
}

#[derive(Debug, StructOpt)]
enum Command {
    #[structopt(name = "list")]
    /// List entries
    List {
        #[structopt(short = "f", long = "format", default_value = "short")]
        /// The format to use when displaying the list. Must be one of
        /// "summary", "short", "report" or "full".
        format: Format,

        #[structopt(short = "d", long = "filter-date")]
        /// Filter the list on the specified YYYY-MM-DD date. Partial dates are
        /// supported, for example --filter-date=2018-04 would return all the
        /// entries for April 2018.
        filter_date: Option<String>,

        #[structopt(short = "t", long = "filter-tag")]
        /// Filter the list on the specified tag.
        filter_tag: Option<String>,

        #[structopt(short = "l", long = "limit", default_value = "0")]
        /// Limit the number of results. Set to 0 to not limit.
        limit: i64,
    },

    #[structopt(name = "record")]
    /// Record a new entry
    Record {
        #[structopt(short = "d", long = "date")]
        /// The date for this record (defaults to today)
        date: Option<NaiveDate>,

        #[structopt(short = "t", long = "tag", value_name = "name")]
        /// An optional tag for the entry (multiple tags may be supplied by
        /// specifying this option several times)
        tags: Vec<String>,

        /// A short summary of the entry
        summary: Vec<String>,
    },
}

#[derive(Debug)]
enum Format {
    Summary,
    Short,
    Report,
    Full,
}

impl FromStr for Format {
    type Err = Error;

    fn from_str(src: &str) -> Result<Self, Self::Err> {
        match src {
            "summary" => Ok(Format::Summary),
            "short" => Ok(Format::Short),
            "report" => Ok(Format::Report),
            "full" => Ok(Format::Full),
            _ => bail!("Invalid format: {}", src),
        }
    }
}

pub fn print_err(msg: &fmt::Display) {
    eprintln!("{} {}", style("Error:").bold().red(), msg);
}

pub fn run() -> Result<(), Error> {
    let args = Cli::from_args();

    let data_dir = get_data_dir(args.data_dir)?;
    create_dir_all(&data_dir)?;

    let jours = super::Jours::new(&data_dir)?;

    match args.cmd {
        Some(Command::Record {
            date,
            tags,
            summary,
        }) => {
            if summary.is_empty() {
                bail!("Summary cannot be empty")
            }

            let date = match date {
                Some(d) => d,
                None => Local::today().naive_local(),
            };
            let summary = summary.join(" ");
            let details = get_text()?;

            jours.record_entry(&summary, details.trim(), &date, &tags)?;
        },
        Some(Command::List {
            format,
            filter_date,
            filter_tag,
            limit,
        }) => {
            let date_range = match filter_date {
                Some(s) => Some(super::utils::parse_date_range(&s)?),
                None => None,
            };

            let entries = jours.get_entries(date_range, filter_tag, limit)?;

            match format {
                Format::Summary => {
                    for entry in entries {
                        println!("* {}", entry.summary);
                    }
                },
                Format::Short => {
                    for entry in entries {
                        println!("* [{}] {}", entry.date, entry.summary);
                    }
                },
                Format::Report => {
                    for entry in entries {
                        println!("*   {}\n", entry.summary);

                        if !entry.details.is_empty() {
                            println!("{}", indent(&entry.details, "    "));
                        }
                    }
                },
                Format::Full => {
                    for entry in entries {
                        println!("*   {}\n", entry.summary);
                        println!("    id: {}", entry.id);
                        println!("    date: {}\n", entry.date);

                        if !entry.details.is_empty() {
                            println!("{}", indent(&entry.details, "    "));
                        }
                    }
                },
            }
        },
        None => {
            let entries = jours.get_entries(None, None, 20)?;

            for entry in entries {
                println!("* [{}] {}", entry.date, entry.summary);
            }
        },
    };

    Ok(())
}

fn get_data_dir(path: Option<String>) -> Result<PathBuf, Error> {
    let data_dir = match path {
        Some(s) => PathBuf::from(s),
        None => xdg_basedir::get_data_home()?.join("jours"),
    };

    Ok(data_dir)
}
