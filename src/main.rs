/* Copyright (c) 2018 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Jours
 *
 * Jours is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Jours is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Jours.  If not, see <http://www.gnu.org/licenses/>.
 */

extern crate chrono;
extern crate console;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate failure;
extern crate plume;
extern crate structopt;
#[macro_use]
extern crate structopt_derive;
extern crate textwrap;
extern crate uuid;
extern crate xdg_basedir;

mod lib;

fn main() {
    let exit_code = match lib::cli::run() {
        Ok(()) => 0,
        Err(e) => {
            lib::cli::print_err(&e);
            1
        },
    };
    std::process::exit(exit_code);
}
