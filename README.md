Jours is a command-line application which lets you record entries and display
them.

It can be used to log things you do at certain dates, for example.

# Usage

```
$ jours record I did something today
$ jours record --date=2018-04-21 I did something else yesterday
$ jours
* [2018-04-22] I did something today
* [2018-04-21] I did something else yesterday
```

See the `--help` option for more details.

# License

Jours is offered under the terms of the
[GNU Affero General Public License, either version 3 or any later version](http://www.gnu.org/licenses/agpl.html).

